var wof = {
    init: function () {
        wof.getWords();
        $('#wrapper').append($("<button/>", {id: "start", html: "Rozpocznij grę"}));
    },
    
    words: null,
    chosenWord: null,
    hintsCounter: 6,
    checkCounter: 3,
    alphabet: "AĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ",
    
    getWords: function () {
        $.ajax({
            type: "GET",
            url: "words.json",
            dataType: 'json',
            beforeSend: wof.showLoader()
        })
                .fail(function () {
                    alert("Nie moge załadować danych");
                })
                .done(function (json) {
                    wof.words = json;
                    wof.hideLoader();
                    wof.startGame();
                });
    },
    
    startGame: function () {
        $('#start').on("click", function () {
            $('#hintCounter').text("Pozostało podpowiedzi: " + wof.hintsCounter);
            $('#checkCounter').text("Pozostało prób odpowiedzi: " + wof.checkCounter);
            $('#wrapper').append($("<div/>", {id: "letterCont"}));
            $('#wrapper').append($("<button/>", {id: "checkBtn", html: "Sprawdź", style: "margin: 20px;"}));
            $('#checkBtn').on("click", wof.checkAnswer);
            $('#wrapper').append($("<br>"));
            $('#wrapper').append($("<button/>", {id: "resetBtn", html: "Zacznij jeszcze raz", style: "margin: 20px;"}));
            $('#resetBtn').on("click", wof.reset);
            wof.shuffle();
            wof.addButtons();
            wof.addChosenWord();
            wof.hideStartButton();
        });
    },
    
    addChosenWord: function () {

        for (var i = 0; i < wof.chosenWord.length; i++) {
            if (wof.chosenWord[i] === " ") {
                var word = $("<br>");
                $('#phraseCont').append(word);
            } else {
                var word = $("<input/>", {
                    class: "phraseLetter",
                    id: [i],
                    maxlength: 1,
                    style: "text-transform:uppercase"
                });
                $('#phraseCont').append(word);
            };
        };
    },
    
    addButtons: function () {
        for (var i = 0; i < wof.alphabet.length; i++) {
            var buttons = $("<button/>", {
                id: wof.alphabet[i],
                html: wof.alphabet[i],
                class: "alphabet"
            });
            buttons.on("click", wof.clickLetter);
            $('#letterCont').append(buttons);
        };
    },
    
    clickLetter: function () {
        wof.hintsCounter--;
        $('#hintCounter').text("Pozostało podpowiedzi: " + wof.hintsCounter);
        if (wof.hintsCounter > 0) {
            $('#' + this.id).attr("disabled", "disable");
            for (var i = 0; i < wof.chosenWord.length - 1; i++) {
                if (this.id === wof.chosenWord[i]) {
                    $('#' + i).val(this.id);
                }
            }
            ;
        } else {
            $('.alphabet').attr("disabled", "disable");

        };
    },
    
    checkAnswer: function () {

        var answerToCheck = " ";
        $(".phraseLetter").each(function () {
            answerToCheck += $(this).val();
        });
        console.log(answerToCheck.toUpperCase().trim());
        console.log(wof.chosenWord.replace(/\s+/g, ""));
        if (wof.checkCounter > 0) {
            if (answerToCheck.toUpperCase().trim() === wof.chosenWord.replace(/\s+/g, "")) {
                alert("Brawo! Prawidłowa odpowiedź!");
                wof.reset();
            } else {
                wof.checkCounter--;
                if (wof.checkCounter > 0) {
                    alert("Źle, próbuj dalej!");
                    $('#checkCounter').text("Pozostało prób odpowiedzi: " + wof.checkCounter);
                } else {
                    alert("Źle, KONIEC GRY - spróbuj jeszcze raz!");
                    wof.reset();
                }
            }
        } else {
            alert("Źle, KONIEC GRY - spróbuj jeszcze raz!");
            wof.reset();
        };
    },
    
    reset: function () {
        $("#phraseCont").text(" ");
        $("#letterCont").text(" ");
        wof.hintsCounter = 6;
        wof.checkCounter = 3;
        $('#hintCounter').text("Pozostało podpowiedzi: " + wof.hintsCounter);
        $('#checkCounter').text("Pozostało prób odpowiedzi: " + wof.checkCounter);
        wof.shuffle();
        wof.addButtons();
        wof.addChosenWord();
    },
    
    showLoader: function () {
        $('#loader').css('display', 'block');
    },
    
    hideLoader: function () {
        $('#loader').css('display', 'none');
    },
    
    hideStartButton: function () {
        $('#start').css('display', 'none');
    },
    
    shuffle: function () {
        wof.chosenWord = wof.words[Math.floor(Math.random() * wof.words.length)];
    }
};

$(document).ready(function () {
    wof.init();
});